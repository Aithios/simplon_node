Exemple Simplon

## Getting started
Modify package.json and add the following dependencies :

```
  "devDependencies": {
    "chai": "^4.1.2",
    "mocha": "^5.2.0"
  }
```

Also add the following script :

```
  "scripts": {
    "test": "mocha"
  },

```

Run npm install into your git repo :

```
npm install
```

To test, then use the following command :

```
npm test
```

## CI/CD
In order to use the **CI/CD** capabilities of Gitlab, you must have a **.gitlab-ci.yml** file in the root directory of your repo. It is used to describe the different stages of your pipeline. This file is already present in this repo as an example, but you are encouraged to test and modify it ! :)

## Server automation
If you want to dig further and deploy your code on real servers, you can take a look at the **ansible-playbook.yml** file to find inspiration, and look at the docs on the ansible website.

## Docs
### Gitlab-ci
https://docs.gitlab.com/ee/ci/yaml/

https://about.gitlab.com/2016/03/01/gitlab-runner-with-docker/
### Gitlab-ci + NodeJS
https://medium.com/@seulkiro/deploy-node-js-app-with-gitlab-ci-cd-214d12bfeeb5
### Chai +  Mocha
http://mherman.org/blog/2015/09/10/testing-node-js-with-mocha-and-chai/

https://buddy.works/guides/how-automate-nodejs-unit-tests-with-mocha-chai

http://www.chaijs.com/api/bdd/
### Systemd + NodeJS
https://blog.codeship.com/running-node-js-linux-systemd/
### Ansible
https://docs.ansible.com/ansible

https://docs.ansible.com/ansible/latest/user_guide/intro_getting_started.html
